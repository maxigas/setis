(ns setis.core (:gen-class))
(require '(clojure.java [io :as io]))

(defn parse-args "Parse command line arguments" [args]
  (if (= 1 (count args))
    (if (= \/ (last (first args))) (first args) (str (first args) "/"))
    ((println "Usage: ./setis.py [https://instagram.com/PROFILENAME]")
     (System/exit 1))))

(defn make-dir "Make directory if it does not exist yet" [name]
  (if (not (.exists (io/file name))) (.mkdir (io/file name))))

(defn unescape "Return string without backslashes" [s]
  (apply str (remove #(= \\ %) s)))

(defn parse "Parse jpg URLs from profile page" [html]
  (distinct (map unescape (remove #(re-find #"s640x640" %)
                                  (re-seq #"https:.*?_[an]\.jpg" html)))))

(def save
  (let [n (atom 0)]
    (fn [total dir url] "Save image from url to dir and print status bar"
      (let [name (str dir "/" (if (= 0 @n) "000_profile_image.jpg"
                                  (last (re-seq #"[0-9_]*_[an].jpg" url))))]
        (swap! n inc)
        (println "Saved:" @n "/" total ":" name)
        (with-open [in (io/input-stream url)
                    out (io/output-stream name)]
          (io/copy in out))))))

(defn -main "Save images from Instagram profile" [& args]
  (let [url (parse-args args)
        name (last (last (re-seq #"/(.*?)/" url)))
        urls (parse (slurp url))
        total (count urls)
        save #(save total name %)]
    (make-dir name)
    (println "Found" total "images on Instagram:" url)
    (save (first urls))
    (dorun (map save (rest urls)))
    (println "Saved" total "images to:" name)))
